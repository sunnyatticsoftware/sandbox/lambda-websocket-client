# lambda-websocket-client

Websocket standard client

## How to use
In a terminal open a web socket connection with the endpoint. Example:
```
wscat -c wss://3xbw4ix9mf.execute-api.eu-west-3.amazonaws.com/Prod
```

Execute this application making sure it connects to the same endpoint and view the message in the console