﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lambda.WebSocket.Client
{
    public class Program
    {
        private const string ApiId = "3xbw4ix9mf";
        private const string Region = "eu-west-3";
        private const string Environment = "Prod";
        
        public static async Task Main(string[] args)
        {
            var cws = new ClientWebSocket();

            var cancelSource = new CancellationTokenSource();
            var connectionUri = new Uri($"wss://{ApiId}.execute-api.{Region}.amazonaws.com/{Environment}");
            await cws.ConnectAsync(connectionUri, cancelSource.Token);

            var message = new ArraySegment<byte>(Encoding.UTF8.GetBytes("{\"message\":\"sendmessage\", \"data\":\"Hello from .NET ClientWebSocket\"}"));
            await cws.SendAsync(message, WebSocketMessageType.Text, true, cancelSource.Token);

            await cws.CloseAsync(WebSocketCloseStatus.NormalClosure, "closed", cancelSource.Token);
        }
    }
}